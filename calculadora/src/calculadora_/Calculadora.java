package calculadora_;

import java.util.Scanner;

public class Calculadora {

	public static void main(String[] args) {
		 Scanner scanner = new Scanner(System.in);

	        System.out.println("Calculadora de Operadores");
	        System.out.println("Ingrese el primer número:");
	        double numero1 = scanner.nextDouble();

	        System.out.println("Ingrese el segundo número:");
	        double numero2 = scanner.nextDouble();

	        System.out.println("Seleccione una operación:");
	        System.out.println("1. Suma");
	        System.out.println("2. Resta");
	        System.out.println("3. Multiplicación");
	        System.out.println("4. División");
	        int opcion = scanner.nextInt();

	        double resultado = 0;

	        switch (opcion) {
	            case 1:
	                resultado = numero1 + numero2;
	                System.out.println("El resultado de la suma es: " + resultado);
	                break;
	            case 2:
	                resultado = numero1 - numero2;
	                System.out.println("El resultado de la resta es: " + resultado);
	                break;
	            case 3:
	                resultado = numero1 * numero2;
	                System.out.println("El resultado de la multiplicación es: " + resultado);
	                break;
	            case 4:
	                if (numero2 != 0) {
	                    resultado = numero1 / numero2;
	                    System.out.println("El resultado de la división es: " + resultado);
	                } else {
	                    System.out.println("No se puede dividir entre cero.");
	                }
	                break;
	            default:
	                System.out.println("Opción no válida.");
	                break;
	        }

	}

}
